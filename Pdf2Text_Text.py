#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import pytesseract
from PIL import Image
import PyPDF2
import docx2txt
import os
import re
import codecs

# Path of working folder on Disk
resource_path = "C:/MyProjects/OCR/"


def saveFileInText(folderPath, filename, text):
    print('Saving file in text format' + filename)
    if not os.path.isdir(folderPath):
        os.mkdir(folderPath)

    path = folderPath + '/' + filename + '.txt'

    with codecs.open(path, "w", "utf-8-sig") as temp:
        temp.write(text)

    print('file saved ' + path)

    #with open(path, 'w') as file:
    #    file.write(text)


def handleFiles(filepath, resource_path, tesserPath):

    textContent = ''

    if (filepath.find(".pdf") > 0 or filepath.find(".PDF") > 0):
        print("pdf file")

        ###first read pdf as text file
        ###if exception then read as image
        try :
            textContent = readAsPDF(filepath)
        except:
            print("unable to read given pdf")
            try:
                textContent = readImageFile(filepath, resource_path, tesserPath)
                #pass
            except Exception as e:
                print("unable to read given image" + str(e))


    elif(filepath.find(".doc") > 0 or filepath.find(".docx") > 0):
        print("reading doc file")
        textContent = readDoc(filepath)
    elif(filepath.find(".jpg") > 0 or filepath.find(".jpeg") > 0 or filepath.find(".png") > 0 or filepath.find(".PNG") ):
        try:
            print("reading image file")
            #raise Exception
            textContent = readImageFile(filepath, resource_path, tesserPath)

        except Exception as e:
            print('Error: not able to read imagefile' + str(e))
    else:
        print("wrong file format")



    return textContent

def readDoc(filepath):
    my_text = docx2txt.process(filepath)

    return my_text


def readAsPDF(filepath):
    pdfFileObj = open(filepath, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    print(pdfReader.numPages)

    num = pdfReader.getNumPages()
    print("total pages: " + str(num))
    pages = []
    for c in range(num):
        pageObj = pdfReader.getPage(c)
        pages.append(pageObj.extractText())

    pdfFileObj.close()

    textData = ', '.join(pages)
    #print(textData)

    return textData


def readImageFile(img_path, resource_path, tesserPath):
    pytesseract.pytesseract.tesseract_cmd = tesserPath + 'tesseract'

    # Read image with opencv
    print(img_path)
    img = cv2.imread(img_path)

    # Convert to gray
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)
    # Write image after removed noise
    cv2.imwrite(resource_path + "removed_noise.png", img)
    #  Apply threshold to get image with only black and white
    #img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)

    # Write the image after apply opencv to do some ...
    cv2.imwrite(resource_path + "thres.png", img)
    # Recognize text with tesseract for python
    result = pytesseract.image_to_string(Image.open(resource_path + "thres.png"))

    # Remove template file
    #os.remove(temp)
    os.remove(resource_path +  "removed_noise.png")
    os.remove(resource_path + "thres.png")
    return result


def getEmailid(textdata):
    emailId = ''
    if re.match(r'[\w\.-]+@[\w\.-]+', textdata):
        print(textdata)

    return emailId

if __name__ == '__main__':
    print('in main')
    #pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
    teaserpath = 'C:/Program Files (x86)/Tesseract-OCR/'
    print('--- Start recognize text from image ---')
    print(handleFiles(resource_path + "cvinImage.jpg", resource_path, teaserpath))

    print("------ Done -------")

