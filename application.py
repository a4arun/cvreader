#!/usr/bin/python
# -*- coding: utf-8 -*-


import os
import Pdf2Text_Text as pt
import MatchProfile as mp
from flask import Flask, request, render_template, send_from_directory



application = Flask(__name__, static_url_path='')
application.secret_key = '2018201920202021'


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
resource_path = os.path.join(application.root_path, 'pdfdata/')
model_path = os.path.join(application.root_path, 'model/')
tesserPath = os.path.join(application.root_path, 'Tesseract-OCR/')


@application.route("/")
def index():
    try:

        dir_name = resource_path + '/textCV/'
        txtFiles = os.listdir(dir_name)

        endStr = ".txt"

        for item in txtFiles:
            if item.endswith(endStr):
                os.remove(os.path.join(dir_name, item))
    except OSError:
        pass
    return render_template("index.html")


@application.route("/upload", methods=["POST"])
def upload():
    folder_name = 'orgi'
    '''
    # this is to verify that folder to upload to exists.
    if os.path.isdir(os.path.join(APP_ROOT, 'files/{}'.format(folder_name))):
        print("folder exist")
    '''

    jdtext = request.form.get('jdtext')
    print(jdtext)
    if jdtext is None or len(jdtext) < 4:
        return render_template("index.html", result="Please enter a valid job description")

    target = os.path.join(APP_ROOT, 'pdfdata/{}'.format(folder_name))
    print(target)
    if not os.path.isdir(target):
        os.mkdir(target)
    print(request.files.getlist("file"))
    filename = ''
    text = ''
    keywordForURL = []

    keywordsJD = mp.finTopWords(jdtext)

    i = 0;
    textArr = []
    fileNameArr = []
    for upload in request.files.getlist("file"):
        print(upload)

        filename = upload.filename
        print(filename)
        # This is to verify files are supported
        #ext = os.path.splitext(filename)[1]
        #if (ext == ".jpg") or (ext == ".png"):
        #    print("File supported moving on...")
        #else:
            #render_template("Error.html", message="Files uploaded are not supported...")
        destination = "/".join([target, filename])
        print("Accept incoming file:", filename)
        print("Save it to:", destination)
        upload.save(destination)

        #now get the text data from pdf, doc, or jpg
        text = pt.handleFiles(destination, resource_path, tesserPath)
        
        #save all data into text format
        pt.saveFileInText(resource_path + 'textCV', filename, text)

        textArr.append(text)
        fileNameArr.append(filename)
        

    #mp.trainModel(model_path, resource_path)
    # build model on given text data on condition
    per = []
    try:
        per = mp.trainModel2(model_path, resource_path, keywordsJD)
    except Exception as e:
        print(e)
    #per = mp.compareCV(sims, keywordsJD)

    print('size of sim arra ' + str(len(per)))
    print('size of textArr ' +  str(len(textArr)))
    j = 0
    for t in textArr:
        # find matching CV on trained model

        keywords = mp.finTopWords(t)
        emailIDs = mp.getEmailId(t)

        # lets take last email id
        emailKey = ''
        for email in emailIDs:
            emailKey = email

        filename = fileNameArr[j]
        jsonInner = {}
        jsonInner[filename] = []

        jsonInner[filename].append(emailKey)
        jsonInner[filename].append(keywords)

        val = 0.0

        if j < len(per):
            val = per[j]

        if val is not None:
            val = val * 100
        jsonInner[filename].append(val)
        j = j + 1


        keywordForURL.append(jsonInner)

    # return send_from_directory("images", filename, as_attachment=True)

    #md = mp.loadModel(model_path=model_path)
    #sims = mp.similarModel(md, jdtext)
    #print(sims)

    #print(keywordForURL)

    #print(keywordForURL[0])
    # sort json
    #lines = sorted(keywordForURL, key=lambda k: k[0].get('val', 0), reverse=True)
    #print(lines)
    urlcounts = len(textArr)
    return render_template("result.html", jdtext=jdtext, data=keywordForURL, urlcounts=str(urlcounts))


@application.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("images", filename)


@application.route('/gallery')
def get_gallery():
    image_names = os.listdir('./images')
    print(image_names)
    return render_template("gallery.html", image_names=image_names)


if __name__ == "__main__":
    application.run(port=5000, debug=True)

