#!/usr/bin/python
# -*- coding: utf-8 -*-

#import cv2
import numpy as np
import pytesseract
from PIL import Image
from rake_nltk import Rake
import RakeKeyExtractor as rke
import gensim
from nltk import RegexpTokenizer
import nltk
from nltk.corpus import stopwords
from os import listdir
from os.path import isfile, join
import re
from nltk.tokenize import word_tokenize
import codecs

def finTopWords(data):
    keywords = rke.extractKey(data)
    return keywords  # r.get_ranked_phrases()

'''
def getTextData(img_path, resource_path, tesserPath):
    pytesseract.pytesseract.tesseract_cmd = tesserPath + 'tesseract'
    # Read image with opencv
    print(img_path)
    img = cv2.imread(img_path)

    # Convert to gray
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    print('---------2----------------')
    img = cv2.dilate(img, kernel, iterations=1)
    print('---------3----------------')
    img = cv2.erode(img, kernel, iterations=1)
    print('---------4----------------')
    # Write image after removed noise
    cv2.imwrite(resource_path + "removed_noise.png", img)
    print('---------5----------------')
    #  Apply threshold to get image with only black and white
    #img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)

    # Write the image after apply opencv to do some ...
    cv2.imwrite(resource_path + "thres.png", img)
    print('---------6----------------')
    # Recognize text with tesseract for python
    result = pytesseract.image_to_string(Image.open(resource_path + "thres.png"))

    # Remove template file
    #os.remove(temp)

    return result

'''
regex = re.compile(("([a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                    "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

def getEmailId(textdata):
    return (email[0] for email in re.findall(regex, textdata) if not email[0].startswith('//'))


def loadModel(model_path):
    # loading the model
    d2v_model = gensim.models.doc2vec.Doc2Vec.load(model_path + 'doc2vec.model')

    docvec = d2v_model.docvecs[1]
    print(docvec)

def similarModel(d2v_model, jdtext):
    sims = d2v_model.docvecs.most_similar(jdtext)

def trainModel2(model_path, resource_path, jdtext):
    path = resource_path + '/textCV/'
    docLabels = []
    docLabels = [f for f in listdir(path) if
                 f.endswith('.txt')]
    # create a list data that stores the content of all text files in order of their names in docLabels
    data = []
    for doc in docLabels:
        with codecs.open(path + doc, "r", "utf-8-sig") as temp:
            data.append(temp.read())

    print(docLabels)

    data = nlp_clean(resource_path, data)
    print('------------------')
    #print(data)

    dictionary = gensim.corpora.Dictionary(data)
    print("Number of words in dictionary:", len(dictionary))
    corpus = [dictionary.doc2bow(gen_doc) for gen_doc in data]
    tf_idf = gensim.models.TfidfModel(corpus)
    print(tf_idf)
    s = 0
    for i in corpus:
        s += len(i)
    print(s)

    sims = gensim.similarities.Similarity(model_path, tf_idf[corpus],
                                          num_features=len(dictionary))
    print(sims)
    print(type(sims))

    print('compare CV')
    query_doc = [w.lower() for w in jdtext]
    print('query doc')
    print(query_doc)
    query_doc_bow = dictionary.doc2bow(query_doc)
    print(query_doc_bow)
    query_doc_tf_idf = tf_idf[query_doc_bow]
    print(query_doc_tf_idf)

    print(sims[query_doc_tf_idf])
    return sims[query_doc_tf_idf]




def trainModel(model_path, resource_path):
    path = resource_path + '/textCV/'
    docLabels = []
    docLabels = [f for f in listdir(path) if
                 f.endswith('.txt')]
    # create a list data that stores the content of all text files in order of their names in docLabels
    data = []
    for doc in docLabels:
        with codecs.open(path + doc, "r", "utf-8-sig") as temp:
            data.append(temp.read())

    print(docLabels)

    data = nlp_clean(data)

    # iterator returned over all documents
    it = LabeledLineSentence(data, docLabels)

    model = gensim.models.Doc2Vec(size=300, min_count=0, alpha=0.025, min_alpha=0.025)
    model.build_vocab(it)

    for epoch in range(100):
        model.train(it, total_examples=120, epochs=50)
        model.alpha -= 0.002
        model.min_alpha = model.alpha

        model.train(it, total_examples=120, epochs=50)
    # saving the created model
    model.save(model_path  + 'doc2vec.model')
    print('model saved')



def nlp_clean(resource_path, data):
    tokenizer = RegexpTokenizer(r'\w+')

    stopwords_set = set(load_stop_words('./SmartStoplist.txt'))
    print(stopwords_set)
    #stopword_set = set(stopwords.words('english'))
    
    new_data = []
    for d in data:
        new_str = d.lower()
        dlist = tokenizer.tokenize(new_str)
        dlist = list(set(dlist).difference(stopwords_set))
        new_data.append(dlist)
    return new_data

def load_stop_words(stop_word_file):

    stop_words = []
    for line in open(stop_word_file):
        if line.strip()[0:1] != "#":
            for word in line.split():  # in case more than one per line
                stop_words.append(word)


    return stop_words

class LabeledLineSentence(object):
    def __init__(self, doc_list, labels_list):
        self.labels_list = labels_list
        self.doc_list = doc_list
    def __iter__(self):
        for idx, doc in enumerate(self.doc_list):
              yield gensim.models.doc2vec.LabeledSentence(doc, [self.labels_list[idx]])



if __name__ == '__main__':
    print('in main')
    pytesseract.pytesseract.tesseract_cmd = resource_path + 'tesseract'

    print('--- Start recognize text from image ---')
    #print(getTextData(resource_path + "cvinImage.jpg"))

    print("------ Done -------")


    application.run(debug=True, threaded=True)

